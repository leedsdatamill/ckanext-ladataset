from .vocabularies import service_list, subject_list, spatial_coverage, update_frequency

import ckan.lib.navl.dictization_functions as df
import ckan.plugins as p
import ckan.plugins.toolkit as tk
import ckan.logic.schema as s

def date_validator(value, context):
    import datetime
    if value == '' or not hasattr(value, 'split'):
        return None

    try:
        d = datetime.datetime.strptime(value, '%d/%m/%Y')
    except Exception, e:
        raise df.Invalid('Date format incorrect')
    return value


class LADatasetPlugin(p.SingletonPlugin, tk.DefaultDatasetForm):
    p.implements(p.IDatasetForm)
    p.implements(p.ITemplateHelpers)
    p.implements(p.IConfigurer)

    def update_config(self, config):
        tk.add_template_directory(config, 'templates')

    def get_helpers(self):
        return {
            'service_list': service_list,
            'subject_list': subject_list,
            'spatial_coverage': spatial_coverage,
            'update_frequency': update_frequency,
        }

    def create_package_schema(self):
        schema = super(LADatasetPlugin, self).create_package_schema()
        schema = self._modify_package_schema(schema)
        return schema

    def update_package_schema(self):
        schema = super(LADatasetPlugin, self).update_package_schema()
        schema = self._modify_package_schema(schema)
        return schema

    def _modify_package_schema(self, schema):
        schema.update({
            'attribution': [tk.get_validator('ignore_missing'),
                            tk.get_converter('convert_to_extras')],
            'next_review_date': [tk.get_validator('ignore_missing'),
                                 date_validator,
                                 tk.get_converter('convert_to_extras')],
            'supported_until': [tk.get_validator('ignore_missing'),
                                date_validator,
                                tk.get_converter('convert_to_extras')],
        })
        schema.update({
            'update_frequency': [
                tk.get_validator('ignore_missing'),
                tk.get_converter('convert_to_tags')('update_frequency')
            ],
            'spatial_coverage': [
                tk.get_validator('ignore_missing'),
                tk.get_converter('convert_to_tags')('spatial_coverage')
            ],
            'service_list': [
                tk.get_validator('ignore_missing'),
                tk.get_converter('convert_to_tags')('service_list')
            ],
            'subject_list': [
                tk.get_validator('ignore_missing'),
                tk.get_converter('convert_to_tags')('subject_list')
            ]
        })
        resource_schema = s.default_resource_schema()
        resource_schema.update({
            'temporal_coverage_from': [tk.get_validator('ignore_missing'),
                                       date_validator,
                                       tk.get_converter('convert_to_extras')],
            'temporal_coverage_to': [tk.get_validator('ignore_missing'),
                                     date_validator,
                                     tk.get_converter('convert_to_extras')],
            'supported_until': [tk.get_validator('ignore_missing'),
                                date_validator,
                                tk.get_converter('convert_to_extras')],
        })
        schema.update({'resources': resource_schema})
        return schema

    def show_package_schema(self):
        schema = super(LADatasetPlugin, self).show_package_schema()
        schema.update({
            'attribution': [tk.get_converter('convert_from_extras'),
                            tk.get_validator('ignore_missing')],
            'next_review_date': [tk.get_converter('convert_from_extras'),
                                 date_validator,
                                 tk.get_validator('ignore_missing')],
            'supported_until': [tk.get_converter('convert_from_extras'),
                                date_validator,
                                tk.get_validator('ignore_missing')],
        })

        schema['tags']['__extras'].append(tk.get_converter('free_tags_only'))
        schema.update({
            'update_frequency': [
                tk.get_converter('convert_from_tags')('update_frequency'),
                tk.get_validator('ignore_missing')],
            'spatial_coverage': [
                tk.get_converter('convert_from_tags')('spatial_coverage'),
                tk.get_validator('ignore_missing')],
            'service_list': [
                tk.get_converter('convert_from_tags')('service_list'),
                tk.get_validator('ignore_missing')],
            'subject_list': [
                tk.get_converter('convert_from_tags')('subject_list'),
                tk.get_validator('ignore_missing')],
            })

        # tk.get_converter('date_to_form')
        schema['resources'].update({
            'temporal_coverage_from': [tk.get_converter('convert_from_extras'),
                                       date_validator,
                                       tk.get_validator('ignore_missing'),
                                     ],
            'temporal_coverage_to': [tk.get_converter('convert_from_extras'),
                                     date_validator,
                                     tk.get_validator('ignore_missing'),
                                     ],
            'supported_until': [tk.get_converter('convert_from_extras'),
                                date_validator,
                                tk.get_validator('ignore_missing')],

        })

        return schema

    def is_fallback(self):
        return True

    def package_types(self):
        return []