import os
import csv

import ckan.plugins.toolkit as tk

def create_update_frequency():
    user = tk.get_action('get_site_user')({'ignore_auth': True}, {})
    context = {'user': user['name']}
    try:
        data = {'id': 'update_frequency'}
        tk.get_action('vocabulary_show')(context, data)
    except tk.ObjectNotFound:
        data = {'name': 'update_frequency'}
        vocab = tk.get_action('vocabulary_create')(context, data)

        options = ['Annually', 'Quarterly', 'Monthly', 'Weekly', 'Daily', 'Hourly', 'Realtime']

        for option in options:
            data = {'name': option, 'vocabulary_id': vocab['id']}
            tk.get_action('tag_create')(context, data)

def create_spatial_coverage():
    user = tk.get_action('get_site_user')({'ignore_auth': True}, {})
    context = {'user': user['name']}
    try:
        data = {'id': 'spatial_coverage'}
        tk.get_action('vocabulary_show')(context, data)
    except tk.ObjectNotFound:
        data = {'name': 'spatial_coverage'}
        vocab = tk.get_action('vocabulary_create')(context, data)

        tags = set()
        pth = os.path.join(os.path.dirname(__file__), 'data/spatial_coverage.csv')

        reader = csv.reader(open(pth))
        for row in reader:
            data = {'name': row[0], 'vocabulary_id': vocab['id']}
            tk.get_action('tag_create')(context, data)

def create_service_list():
    user = tk.get_action('get_site_user')({'ignore_auth': True}, {})
    context = {'user': user['name']}
    try:
        data = {'id': 'service_list'}
        tk.get_action('vocabulary_show')(context, data)
    except tk.ObjectNotFound:
        data = {'name': 'service_list'}
        vocab = tk.get_action('vocabulary_create')(context, data)

        tags = set()
        pth = os.path.join(os.path.dirname(__file__), 'data/englishAndWelshServices.csv')

        reader = csv.DictReader(open(pth, 'r'))
        for row in reader:
            l = row['Label'].replace(',', '').replace("'","").replace('/','-').replace('(', '').replace(')', '').replace('&', 'and')
            tags.add(l)

        for tag in tags:
            data = {'name': tag, 'vocabulary_id': vocab['id']}
            try:
                tk.get_action('tag_create')(context, data)
            except:
                print 'Failed to add ', tag

def create_subject_list():
    user = tk.get_action('get_site_user')({'ignore_auth': True}, {})
    context = {'user': user['name']}
    try:
        data = {'id': 'subject_list'}
        tk.get_action('vocabulary_show')(context, data)
    except tk.ObjectNotFound:
        data = {'name': 'subject_list'}

        tags = set()
        pth = os.path.join(os.path.dirname(__file__), 'data/subjects_services.csv')
        reader = csv.DictReader(open(pth, 'r'))
        for row in reader:
            if row['Type'] != 'Subject':
                continue
            if row['Level 1'] != "":
                continue
            l = row['Label'].replace(',', '').replace("'","").replace('/','-').replace('(', '').replace(')', '').replace('&', 'and')
            tags.add(l)

        vocab = tk.get_action('vocabulary_create')(context, data)
        for tag in tags:
            data = {'name': tag, 'vocabulary_id': vocab['id']}
            tk.get_action('tag_create')(context, data)

def spatial_coverage():
    create_spatial_coverage()
    try:
        tag_list = tk.get_action('tag_list')
        slist = tag_list(data_dict={'vocabulary_id': 'spatial_coverage'})
        return slist
    except tk.ObjectNotFound:
        return None

def service_list():
    create_service_list()
    try:
        tag_list = tk.get_action('tag_list')
        slist = tag_list(data_dict={'vocabulary_id': 'service_list'})
        return slist
    except tk.ObjectNotFound:
        return None

def subject_list():
    create_subject_list()
    try:
        tag_list = tk.get_action('tag_list')
        slist = tag_list(data_dict={'vocabulary_id': 'subject_list'})
        return slist
    except tk.ObjectNotFound:
        return None

def update_frequency():
    create_update_frequency()
    try:
        tag_list = tk.get_action('tag_list')
        slist = tag_list(data_dict={'vocabulary_id': 'update_frequency'})
        return slist
    except tk.ObjectNotFound:
        return None
