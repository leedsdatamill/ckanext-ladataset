# README 

This extension is specifically for the use of UK Local Authorities and contains a metadata schema specifically for them as it includes information from LGA (Local Government Association).



### Installation

* Install the code

```
git@bitbucket.org:leedsdatamill/ckanext-ladataset.git
cd ckanext-ladataset
python setup.py develop
```
* Add ```ladataset``` to plugins in ckan.ini

The first time a user edits a dataset it will populate the data for a few fields, and so may be slow. Speeding up on subsequent edits (at the usual speed).

