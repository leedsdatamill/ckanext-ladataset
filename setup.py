from setuptools import setup, find_packages
import sys, os

version = '1.0'

setup(
    name='ckanext-ladataset',
    version=version,
    description="Datasets containing schema for local authorities",
    long_description='''
    ''',
    classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
    keywords='',
    author='Ross Jones',
    author_email='ross@servercode.co.uk',
    url='https://bitbucket.org/leedsdatamill/ckanext-ladataset',
    license='AGPL',
    packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
    namespace_packages=['ckanext', 'ckanext.ladataset'],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        # -*- Extra requirements: -*-
    ],
    entry_points='''
        [ckan.plugins]
        ladataset=ckanext.ladataset.plugin:LADatasetPlugin
    ''',
)
